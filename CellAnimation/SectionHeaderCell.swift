//
//  AppDelegate.swift
//  CellAnimation
//
//  Created by PrahladReddy on 1/10/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//


import UIKit

class SectionHeaderCell: UITableViewCell {

    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var sectionSubtitleLabel: UILabel!

    func setup(model: SectionModel) {
        sectionTitleLabel.text = model.sectionTitle
        sectionSubtitleLabel.text = model.sectionSubtitle
    }
}
