//
//  AppDelegate.swift
//  CellAnimation
//
//  Created by PrahladReddy on 1/10/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    
    func setup(model: Model) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subTitle
        cellImageView.image = model.image
        
        cardView.layer.shadowOpacity = 0.35
        cardView.layer.shadowRadius = 3
        cardView.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        cellImageView.layer.cornerRadius = 30
    }
    
    func animateSwipeHint() {
        let swipeHintDistance: CGFloat = 30
        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: [.curveEaseOut], animations: {
            // Slide in from right
            self.cellBackgroundView.transform = CGAffineTransform(translationX: -swipeHintDistance, y: 0)
            self.cellBackgroundView.layer.cornerRadius = 10
        }) { (success) in
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear], animations: {
                self.cellBackgroundView.transform = .identity
                self.cellBackgroundView.layer.cornerRadius = 0
            }, completion: { (success) in
                UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                    // Slide in from left
                    self.cellBackgroundView.transform = CGAffineTransform(translationX: swipeHintDistance, y: 0)
                    self.cellBackgroundView.layer.cornerRadius = 10
                }) { (success) in
                    UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear], animations: {
                        self.cellBackgroundView.transform = .identity
                        self.cellBackgroundView.layer.cornerRadius = 0
                    })
                }
            })
        }
    }
}
