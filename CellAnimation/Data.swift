//
//  ViewController.swift
//  CellAnimation
//
//  Created by EPITADMBP04 on 1/10/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//


/*
 This is where you will be getting your data from a different source.
 You can arrange this any way you want. Here, each section you get will also get the rows that go with it.
 */

import UIKit

class Data {
    
    static func getData(completion: @escaping ([SectionModel]) -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            var data = [SectionModel]()
            data.append(SectionModel(sectionTitle: "Pending", sectionSubtitle: "You decide who is in or out", data: getModel(sectionTitle: "Pending")))
            data.append(SectionModel(sectionTitle: "Accepted", sectionSubtitle: "People in your inner circle", data: getModel(sectionTitle: "Accepted")))
            data.append(SectionModel(sectionTitle: "Rejected", sectionSubtitle: "Never to be seen from again", data: getModel(sectionTitle: "Rejected")))
            data.append(SectionModel(sectionTitle: "Waitlist", sectionSubtitle: "Take some time and think about it", data: getModel(sectionTitle: "Waitlist")))
            DispatchQueue.main.async {
                completion(data)
            }
        }
    }
    
    static func getModel(sectionTitle: String) -> [Model] {
        var data = [Model]()
        
        switch sectionTitle {
        case "Pending":
            data.append(Model(title: "Eve", subTitle: "Eve's info", image: #imageLiteral(resourceName: "Eve"), data1: "Row 1 Data 1", data2: "Row 1 Data 2"))
            data.append(Model(title: "Jaqueline", subTitle: "Jaqueline's info", image: #imageLiteral(resourceName: "Jaqueline"), data1: "Row 2 Data 1", data2: "Row 2 Data 2"))
            data.append(Model(title: "Julie", subTitle: "Julie's info", image: #imageLiteral(resourceName: "Julie"), data1: "Row 3 Data 1", data2: "Row 3 Data 2"))
            data.append(Model(title: "Rod", subTitle: "Rod's the iOS Developer", image: #imageLiteral(resourceName: "Robert"), data1: "Row 3 Data 1", data2: "Row 3 Data 2"))
            data.append(Model(title: "Sarah", subTitle: "Sarah's info", image: #imageLiteral(resourceName: "Sarah"), data1: "Row 3 Data 1", data2: "Row 3 Data 2"))
            data.append(Model(title: "Chase", subTitle: "Chase, the Android Developer", image: #imageLiteral(resourceName: "Stewart"), data1: "Row 3 Data 1", data2: "Row 3 Data 2"))
            data.append(Model(title: "Lucy", subTitle: "Lucy's info", image: #imageLiteral(resourceName: "Lucy"), data1: "Row 2 Data 1", data2: "Row 2 Data 2"))
            data.append(Model(title: "Durtschi", subTitle: "AKA 'Dirty'", image: #imageLiteral(resourceName: "Matthew"), data1: "Row 3 Data 1", data2: "Row 3 Data 2"))
        case "Accepted":
            data.append(Model(title: "Maya", subTitle: "Maya's info", image: #imageLiteral(resourceName: "Maya"), data1: "Row 1 Data 1", data2: "Row 1 Data 2"))
        case "Rejected":
            data.append(Model(title: "Julius", subTitle: "Julius' info", image: #imageLiteral(resourceName: "Julius"), data1: "Row 1 Data 1", data2: "Row 1 Data 2"))
        default:
            data.append(Model(title: "Patty", subTitle: "Patty's info", image: #imageLiteral(resourceName: "Patty"), data1: "Row 2 Data 1", data2: "Row 2 Data 2"))
            data.append(Model(title: "Rick", subTitle: "Rick's info", image: #imageLiteral(resourceName: "Rick"), data1: "Row 3 Data 1", data2: "Row 3 Data 2"))

        }
        
        return data
    }
}
