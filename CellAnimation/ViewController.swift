//
//  AppDelegate.swift
//  CellAnimation
//
//  Created by PrahladReddy on 1/10/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tableData: [SectionModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        Data.getData { (data) in
            self.tableData = data
            self.tableView.reloadData()
            
            if self.tableView.visibleCells.count > 0 {
                let cell = self.tableView.visibleCells[0] as! TableViewCell
                cell.animateSwipeHint()
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section].data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell") as! TableViewCell
        cell.setup(model: tableData[indexPath.section].data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if indexPath.section != 0 { return nil }
        
        let accept = UIContextualAction(style: .normal, title: "Accept") { (action, view, nil) in
            
            print("Accepted")
            tableView.reloadData()
        }
        accept.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        accept.image = #imageLiteral(resourceName: "Accept")
        
        let waitlist = UIContextualAction(style: .normal, title: "Waitlist") { (action, view, nil) in
            print("Waitlisted")
            //tableView.reloadData()
        }
        waitlist.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        waitlist.image = #imageLiteral(resourceName: "Waitlist")
        
        let config = UISwipeActionsConfiguration(actions: [accept, waitlist])
        config.performsFirstActionWithFullSwipe = false // Prevent swiping
        return config
    }
    func setDefaultStyle(view : UIImageView) {
        view.image = view.image!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        view.tintColor = UIColor.blue
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, view, success:(Bool) -> Void) in
            self.tableData[indexPath.section].data.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            success(true)
        }
        delete.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        delete.image = #imageLiteral(resourceName: "Delete")
        
        let reject = UIContextualAction(style: .destructive, title: "Rejected") { (action, view, nil) in
            print("Rejected")
        }
        reject.backgroundColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        reject.image = #imageLiteral(resourceName: "Reject")
        
        if indexPath.section == 0 {
            return UISwipeActionsConfiguration(actions: [delete, reject])
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionHeaderCell") as! SectionHeaderCell
        cell.setup(model: tableData[section])
        return cell.contentView
    }
}
