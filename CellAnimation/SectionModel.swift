//
//  ViewController.swift
//  CellAnimation
//
//  Created by EPITADMBP04 on 1/10/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//


import UIKit

class SectionModel {
    var sectionTitle = ""
    var sectionSubtitle = ""
    var data: [Model] = []
    
    init(sectionTitle: String, sectionSubtitle: String, data: [Model]) {
        self.sectionTitle = sectionTitle
        self.sectionSubtitle = sectionSubtitle
        self.data = data
    }
}
